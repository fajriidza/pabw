<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('utama');
Route::get('/maps', function () {
    return view('maps');
});


Auth::routes();

//Admin
Route::get('/admin/login', 'Auth\AdminLoginController@showLoginForm')->name('admin.login')->middleware('guest');
Route::post('/admin/login', 'Auth\AdminLoginController@login')->name('admin.login.submit');
Route::delete('/kelolaPetugas/{hapus}', 'AdminController@destroy')->name('petugas.destroy');
Route::get('/kelolaPetugas', 'AdminController@kelolaPetugas')->name('kelolaPetugas');
Route::post('/kelolaPetugas', 'AdminController@store');
Route::patch('/kelolaPetugas/{kirim}','AdminController@update')->name('petugas.update');



//User
Route::get('/daftar', 'Auth\UserRegisterController@showRegistrationForm')->name('user.register')->middleware('guest');
Route::post('/daftar/{kirim}', 'Auth\UserRegisterController@store')->name('daftar');
Route::get('/donatur/donasi', 'DonasiController@donasiSekarang')->middleware('auth:web');
Route::get('/login', 'Auth\UserLoginController@showLoginForm')->name('user.login')->middleware('guest');
Route::post('/login', 'Auth\UserLoginController@login')->name('user.login.submit');
Route::get('/donatur', 'UserController@index')->name('dashboarduser');
Route::post('/donatur/edit/{id}', 'UserController@editProfil')->name('editProfile');
Route::get('/donatur/riwayat/{id}', 'UserController@riwayatDonasi');
Route::post('/donatur/riwayat/{id}', 'UserController@riwayatDonasi')->name('riwayatDonasi');
Route::put('/donatur/edit/{id}', 'UserController@updateProfil')->name('profil.update');
Route::get('/bencana','HomeController@daftarBencana');
Route::get('/detail/{id}','HomeController@detailbencana')->name('detail.bencana');
Route::get('/donatur/donasi/{id}','DonasiController@donasibencana')->name('donasi.bencana')->middleware('auth:web');
Route::post('/donatur/donasi', 'DonasiController@storeDonasi')->name('tambah.donasi')->middleware('auth:web');



//Petugas

Route::get('/petugas/login', 'Auth\PetugasLoginController@showLoginForm')->name('petugas.login')->middleware('guest');
Route::post('/petugas/login', 'Auth\PetugasLoginController@login')->name('petugas.login.submit');
Route::get('/petugas/verifikasi','PetugasController@verifikasi')->name('verif.donasi')->middleware('auth:petugas');;
Route::get('/petugas/verifikasi/{id}','PetugasController@editVerifikasi')->name('verif.edit')->middleware('auth:petugas');;
Route::get('/petugas/jemput', 'PetugasController@jemput')->name('jemput.donasi');
Route::get('/petugas/jemput/{id}','PetugasController@editJemput')->name('jemput.edit')->middleware('auth:petugas');;
Route::patch('/petugas/verifikasi/{id}','PetugasController@verifikasiDonasi')->name('verifikasi');
Route::get('/petugas/donatur', 'PetugasController@dataDonatur');
Route::get('/petugas/distribusi', 'PetugasController@distribusi')->name('distribusi.donasi');
Route::patch('/petugas/distribusi/{kirim}','PetugasController@distribusiDonasi')->name('distribusi');
Route::patch('/petugas/jemput/{id}','PetugasController@atur_jadwal')->name('petugas.atur_jadwal');
Route::patch('/petugas/verifikasi/{id}','PetugasController@simpanVerifikasi')->name('simpan.verifikasi');



//Petugas Bencana
Route::post('/petugas/bencana', 'PetugasController@store')->name('bencana.store');
Route::get('/petugas/bencana', 'PetugasController@bencana')->name('infobencana')->middleware('auth:petugas');
Route::get('petugas/bencana/{id}', 'PetugasController@editBencana')->name('bencana.edit');
Route::put('/petugas/bencana/{id}','PetugasController@updateBencana')->name('bencana.update');
Route::delete('/petugas/bencana/{hapus}', 'PetugasController@destroyBencana')->name('bencana.destroy');
Route::post('select-ajax', ['as'=>'select-ajax','uses'=>'LokasiController@selectAjax']);


Route::get('/temu', 'HomeController@coba');
Route::get('/cari', 'HomeController@loadData');

Route::get('/welcome', function () {
    return view('welcome');
});

Route::get('/daftar1', function () {
    return view('daftar');
});

Route::get('/formdonasi', function () {
    return view('formdonasi');
});

Route::get('/loginAdmin', function () {
    return view('loginAdmin');
});

Route::get('/loginPetugas', function () {
    return view('loginPetugas');
});

Route::get('/riwayatDonasi', function () {
    return view('riwayatDonasi');
});

Route::get('/detailBencana', function () {
    return view('detailBencana');
});

Route::get('/daftarBencana', function () {
    return view('daftarBencana');
});

Route::get('/loginD', function () {
    return view('loginD');
});

Route::get('/editProfil', function () {
    return view('editProfil');
});

Route::get('/udahLogin', function () {
    return view('udahLogin');
});

Route::get('/dAdmin', function () {
    return view('dAdmin');
});

Route::get('/dPetugas', function () {
    return view('dPetugas');
});

Route::get('/kelolaPetugas1', function () {
    return view('kelolaPetugas');
});

Route::get('/jBantuan', function () {
    return view('jBantuan');
});

Route::get('/infBencana', function () {
    return view('infBencana');
});

Route::get('/dDonatur', function () {
    return view('dDonatur');
});

Route::get('/verifDonasi', function () {
    return view('verifDonasi');
});

Route::get('/diBantuan', function () {
    return view('diBantuan');
});

Route::get('editInfBencana', function () {
    return view('editInfBencana');
});

Route::get('/panduanDonasi', function () {
    return view('panduanDonasi');
});

Route::get('/editVerif', function () {
    return view('editVerif');
});

Route::get('/editJadwal', function () {
    return view('editJadwal');
});

Route::get('/detailDonatur', function () {
    return view('detailDonatur');
});