<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\provinces;
use App\regencies;
use DB;
use Hash;
use Illuminate\Support\Facades\Validator;

class UserRegisterController extends Controller
{
	protected $redirectTo = '/';
    public function showRegistrationForm()
    {
    	$provinces = DB::table('provinces')->orderBy('provinces.name')->pluck("name","id")->all();
        return view('User.daftar',compact('provinces'));
    }
    public function store(Request $request)
    {
        $user = User::all();

        
    	$validasi = Validator::make($request->all(),[
            'password' => 'min:6|required_with:repassword|same:repassword',
			'repassword' => 'min:6'
        ]);

        if (is_null($user)){
            if($validasi->passes()){

                $user = new User();
                $user->name = $request->nama;
                $user->no_hp = $request->no_hp;
                $user->alamat = $request->alamat;
                $user->lokasi_id = $request->id_districts;
                $user->email = $request->email;
                $user->password = Hash::make($request->password);
                $user->save();
                return redirect('/login');
            }
            else{
                 return redirect()->to('/daftar')->with('alert','Password dan Konfirmasi Password Berbeda')->withInput($request->only('email'));
                }
        }
        else{

            foreach ($user as $user ) {

            if ($user->email == $request->email){

                 return redirect()->to('/daftar')->with('alert','Email sudah digunakan')->withInput($request->only('email'));
            }
            else if($validasi->passes()){

                $user = new User();
                $user->name = $request->nama;
                $user->no_hp = $request->no_hp;
                $user->alamat = $request->alamat;
                $user->lokasi_id = $request->id_districts;
                $user->email = $request->email;
                $user->password = Hash::make($request->password);
                $user->save();
                return redirect('/login');
            }
            else{
                 return redirect()->to('/daftar')->with('alert','Password dan Konfirmasi Password Berbeda')->withInput($request->only('email'));
            }
        }


                
        }
       
        


    }

}
