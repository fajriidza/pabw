<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Petugas;
use App\Admin;
use App\provinces;
use App\Categori;
use App\regencies;
use App\Bencana;
use App\Donasi;
use App\User;
use DB;

class PetugasController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:petugas');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function dashboard()
    {
        return view('Petugas.dPetugas');
    }
    public function bencana(Request $request)
    {
        $search = $request->input('search');
        $petugas = Petugas::all();
        $bencana = Bencana::search($search)->orderBy('bencana.id','desc')->paginate(20);
        //$regencies = DB::table('regencies')->pluck("name","id")->all();
         $provinces = DB::table('provinces')->orderBy('provinces.name')->pluck("name","id")->all();

        return view('Petugas.infBencana',compact('regencies','provinces','bencana','petugas','search'));
    }

     public function store(Request $request)
    {

        
        $this->validate($request, [

        'gambar' => 'required',
        'gambar.*' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'

    ]);
      function generateRandomString($length = 10) {
      $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
      $charactersLength = strlen($characters);
      $randomString = '';
      for ($i = 0; $i < $length; $i++) {
          $randomString .= $characters[rand(0, $charactersLength - 1)];
      }
      return $randomString;
  }
      if($request->hasfile('gambar'))
       {

        $image=  $request->file('gambar');

              $name=generateRandomString().'.'.$image->getClientOriginalExtension();
              $image->move(public_path().'/img/bencana/', $name);
              $data = $name;

       }
       $bencana= new Bencana();
    
            $bencana->nama_bencana = $request->NamaBencana;
            $bencana->lokasi_id = $request->id_regencies;
            $bencana->kebutuhan = implode(",",$request->kebutuhan);
            $bencana->deskripsi_bencana = $request->deskripsi_bencana;
            $bencana->petugas_id = $request->petugas_id;
            $bencana->gambar = $data;
            $bencana->save();
        
        return redirect()->to('/petugas/bencana')->with('alert','Berhasil !!');
        
    }
    public function verifikasi(Request $request)
    {
        $search = $request->input('search');
        $donasi = Donasi::search($search)->where('donasi.status','Menunggu')->paginate(20);;

        return view('Petugas.verifDonasi',compact('donasi','search'));

    }
    public function verifikasiDonasi($id)
    {
        //$id = $request->donasi_id;
        $donasi = DB::table('donasi')->where('id',$id);
        $donasi->update([
            'status' => request('status')

        ]);
        return redirect('/petugas/verifikasi');

    }

    public function editVerifikasi($id)
    {
        $bencana= Bencana::all();
        $donasi = Donasi::findOrFail($id);
        
        return view('Petugas.editVerif',compact('donasi','bencana'));
    }
    public function simpanVerifikasi($id, Request $request)
    {
        $donasi = DB::table('donasi')->where('id',$id);
        $idbencana= $request->id_bencana;
        $bencana= DB::table('bencana')->where('id',$idbencana);
        //$lokasi = $bencana->lokasi_id;
         $donasi->update([
            'bencana_id' => request('id_bencana'),
            'lokasi_id'  => request('lokasi_bencana')

        ]);
        return redirect('/petugas/verifikasi');
    }

    public function jemput(Request $request)
    {
        $search = $request->input('search');
        $donasi = Donasi::search($search)->where('donasi.status','Komitmen')->orWhere('donasi.status','Siap Jemput')->paginate(20);
        return view('Petugas.jBantuan',compact('donasi','search'));

    }
    public function editJemput($id)
    {
        $donasi = Donasi::findOrFail($id);
        
        return view('Petugas.editJadwal',compact('donasi'));

    }

    public function atur_jadwal($id)
    {
       
        $donasi = Donasi::findOrFail($id);
        $donasi->update([
            'jadwal_jemput' => request('jadwal'),
            'waktu_jemput' => request('waktu'),
            'status' => request('status')

        ]);
        return redirect('/petugas/jemput');
    }

   


    public function dataDonatur()
    {
        $donasi= Donasi::all();

        return view('Petugas.dDonatur',compact('donasi'));
    }

    public function distribusi(Request $request)
    {
        $search = $request->input('search');
        $donasi = Donasi::search($search)->where('donasi.status','Telah Dijemput')->paginate(20);;

        return view('Petugas.diBantuan',compact('donasi','search'));
    }
    public function distribusiDonasi(Request $request)
    {
        $id = $request->donasi_id;
        $donasi = DB::table('donasi')->where('id',$id);
        $donasi->update([
            'status' => request('status')

        ]);
        return redirect('/petugas/distribusi');

    }

    public function editBencana($id)
    {
        $bencana = Bencana::findOrFail($id);
        $provinces = DB::table('provinces')->pluck("name","id")->all();
        $regencie = regencies::all();
        $kabupaten=DB::table('regencies')->where('province_id',$bencana->regencies->provinces['id'])->pluck("name","id")->all();
        return view('Petugas.editInfBencana', compact('provinces','regencie','bencana','kabupaten'));

    }
     public function destroyBencana(Request $request)
    {
           $bencana = Bencana::findOrFail($request->bencanaIdDelete);
            $bencana->delete();
            return redirect('/petugas/bencana');

    }

    public function updateBencana(Request $request, $id)
    {

        $this->validate($request, [


        'gambar.*' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'

            ]);
              function generateRandomString($length = 10) {
              $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
              $charactersLength = strlen($characters);
              $randomString = '';
              for ($i = 0; $i < $length; $i++) {
                  $randomString .= $characters[rand(0, $charactersLength - 1)];
              }
              return $randomString;
          }
      if($request->hasfile('gambar'))
       {

            $image=  $request->file('gambar');

              $name=generateRandomString().'.'.$image->getClientOriginalExtension();
              $image->move(public_path().'/img/bencana/', $name);
              $data = $name;

       }
        $bencana = Bencana::find($id);
        $bencana->nama_bencana = $request->nama_bencana;
        $bencana->deskripsi_bencana = $request->deskripsi;
        $bencana->kebutuhan = $request->kebutuhan;
        $bencana->lokasi_id = $request->id_regencies;
        if($request->hasfile('gambar')){
            $bencana->gambar = $data;
        }
        $bencana->save();

        return redirect('petugas/bencana');
    }

    public function autoBencana(Request $request)
    {/*

         if($request->ajax()){
            $bencana = DB::table('bencana')->where('id',$request->nama_bencana)->pluck("nama_bencana","id")->all();
            $deskripsi = $bencana->deskripsi ;
            return response()->json($deskripsi);
        }

        /*
         if($request->get('query'))
         {
          $query = $request->get('query');
          $data = DB::table('bencana')
            ->where('nama_bencana', 'LIKE', "%{$query}%")
            ->get();
          $output = '<ul class="dropdown-menu form-control" style="display:block; position:relative">';
          foreach($data as $row)
          {
           $output .= '
           <div class="form-control">
           <li><a href="#">'.$row->nama_bencana.'</a></li>
           </div>
           ';
          }
          $output .= '</ul>';
          echo $output;
         }*/
    }
}
