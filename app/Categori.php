<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categori extends Model
{
    protected $fillable = ['id','kategori_nama'];
    protected $table ='categoris';

 
    public function Donasi()
    {
        return $this->hasOne('App\Donasi','jenis_donasi');
    }
}
