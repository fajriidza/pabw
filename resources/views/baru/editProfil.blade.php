<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/editProfil.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">

    <title>Hello, world!</title>
  </head>
     <body>
      <!-- navbar -->
      <nav class="navbar fixed-top navbar-expand-lg navbar-light bg-light">
          <div class="container-fluid">
          <a class="navbar-brand" href="#">DONATE</a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarNav">
              <ul class="navbar-nav ml-auto">
              <li class="nav-item">
                  <a class="nav-link" href="/welcome">Beranda <span class="sr-only">(current)</span></a>
              </li>
              <li class="nav-item">
                  <a class="nav-link" href="#">Panduan Donasi</a>
              </li>
              <li class="nav-item">
                  <a class="btn btn-outline-primary ds" href="/formdonasi">Donasi Sekarang</a>
              </li>
              <li class="nav-item">
                <div class="btn-group lg" role="group">
                  <button id="btnGroupDrop1" type="button" class="btn dropdown-toggle btn-primary lg" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="fas fa-user-circle"></i>  Mr. Ahmad Daidfaids
                  </button>
                  <div class="dropdown-menu dropdown-menu-right" aria-labelledby="btnGroupDrop1">
                    <a class="dropdown-item" href="/loginPetugas">Edit Profile</a>
                    <a class="dropdown-item" href="/login">Riwayat Donasi</a>
                    <a class="dropdown-item logout" href="/login">Log Out</a>

                  </div>
                </div>
                 <!--  <a class="btn btn-outline-primary lg" href="/login">Masuk</a> -->
              </li>
              </ul>
          </div>
          </div>
      </nav>
    <!-- akhir navbar -->
    <div class="jumbotron jumbotron-fluid">
      <div class="container-fluid">

      <div class="row">

        <div class="col-sm-8 offset-sm-2 form ">

              <h2 class="text-center">Edit Donatur</h2>
              <!-- <div class="col-sm-12">
                <div class="row">
                  <div class="col-md-6">
                    <label for="">Nama</label>
                  <input type="text" name="" value="" class="form-control" placeholder="Masukkan Nama Lengkap Anda">
                </div>
                <div class="col-md-6">
                  <label for="">Email</label>
                  <input type="email" name="" value="" class="form-control" placeholder="Masukkan Email Anda">
                </div>
                </div>
                <div class="row">
                  <div class="col-md">
                    <label for="">Alamat</label>
                    <textarea name="name" class="form-control"></textarea>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md">
                    <label for="">Provinsi</label>
                    <select class="form-control" name="">
                      <option value="">DKI Jakarta</option>
                    </select>
                  </div>
                  <div class="col-md">
                    <label for="">Kabupaten/Kota</label>
                    <select class="form-control" name="">
                      <option value="">Jakarta Pusat</option>
                    </select>
                  </div>
                  <div class="col-md">
                    <label for="">Kecamatan</label>
                    <select class="form-control" name="">
                      <option value="">Tanah Abang</option>
                    </select>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md">
                    <label for="">Password</label>
                    <input type="password" name="" value="" class="form-control" placeholder="Masukkan Password Anda">
                  </div>
                  <div class="col-md">
                    <label for="">Ulangi Password</label>
                    <input type="password" name="" value="" class="form-control" placeholder="Ulangi Password">
                  </div>
                </div> -->

                <div class="row">
                  <div class="col-sm-6">
                    <label for="">Nama Lengkap</label>
                    <input type="text" class="form-control" name="" value="" placeholder="Masukkan Nama Lengkap Anda">
                  </div>
                  <div class="col-sm-6">
                    <label for="">Provinsi</label>
                    <select class="form-control" name="">
                      <option value="">Pilih Provinsi</option>
                      <option value="">DKI Jakarta</option>
                    </select>
                  </div>
                </div>

                <div class="row">
                  <div class="col-sm-6">
                        <label for="">Email Anda</label>
                        <input type="text" class="form-control" name="" value="" placeholder="Masukkan Email Anda">
                      </div>
                  <div class="col-sm-6">
                    <label for="">Kabupaten/Kota</label>
                    <select class="form-control" name="">
                      <option value="">Pilih Kabupaten/Kota</option>
                      <option value="">Jakarta Pusat</option>
                    </select>
                  </div>
                </div>

                <div class="row">
                  <div class="col-sm-6">
                    <label for="">Nomor Telepon</label>
                    <input type="text" class="form-control" name="" value="" placeholder="Masukkan Nomor Telepon Anda">
                  </div>
                  <div class="col-sm-6">
                    <label for="">Kecamatan</label>
                    <select class="form-control" name="">
                        <option value="">Pilih Kecamatan</option>
                      <option value="">Tanah Abang</option>
                    </select>
                  </div>
                </div>

                <div class="row">
                  <div class="col-sm-6 ">
                    <label for="" class="inputAlamat">Alamat Lengkap</label>
                    <textarea name="name"  class="form-control"  placeholder="Masukkan Alamat Lengkap Anda"></textarea>
                  </div>
                  <div class="col-sm-6"
                    <label for="">Password</label>
                    <input type="password" name="" class="form-control" value="" placeholder="Masukkan Password Anda">
                    <label for="">Ulangi Password</label>
                    <input type="password" name="" class="form-control" value=""  placeholder="Ulangi Password Anda">
                  </div>
                </div>

                <br>
                <div class="row">
                  <div class="col-sm-4 offset-sm-4 btnSubmit">
                    <button class="btn btn-lg btn-primary btn-block" type="submit">Simpan</button>
                  </div>
                </div>
              </div>
            </div>

                 <!-- footer -->
                 <footer>
                   <div class="row">
                     <div class="col-10 offset-1 text-center">
                       <p>&copy; Copyright 2018 | Built with by <span>De Nun </span><br>Jalan Kaliurang Km. 14,5, Yogyakarta, Krawitan, Umbulmartani, Ngemplak, Kabupaten Sleman, <br>Daerah Istimewa Yogyakarta 55584 </p>
                     </div>
                   </div>
                 </footer>
                <!-- akhir footer -->
              </div>
    </div>

    </body>


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script type="text/javaScript" src="js/bootstrap.min.js" ></script>
    <script type="text/javaScript" src="js/bootstrap.min.js" ></script>
  </body>
</html>
