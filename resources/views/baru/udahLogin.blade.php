<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/udahLogin.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">

    <title>Hello, world!</title>
  </head>
     <body>
      <!-- navbar -->
      <nav class="navbar fixed-top navbar-expand-lg navbar-light bg-light">
          <div class="container-fluid">
          <a class="navbar-brand" href="#">DONATE</a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarNav">
              <ul class="navbar-nav ml-auto">
              <li class="nav-item">
                  <a class="nav-link" href="/welcome">Beranda <span class="sr-only">(current)</span></a>
              </li>
              <li class="nav-item">
                  <a class="nav-link" href="#">Panduan Donasi</a>
              </li>
              <li class="nav-item">
                  <a class="btn btn-outline-primary ds" href="/formdonasi">Donasi Sekarang</a>
              </li>
              <li class="nav-item">
                <div class="btn-group lg" role="group">
                  <button id="btnGroupDrop1" type="button" class="btn dropdown-toggle btn-primary lg" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="fas fa-user-circle"></i>  Mr. Ahmad Daidfaids
                  </button>
                  <div class="dropdown-menu dropdown-menu-right" aria-labelledby="btnGroupDrop1">
                    <a class="dropdown-item" href="/loginPetugas">Edit Profile</a>
                    <a class="dropdown-item" href="/login">Riwayat Donasi</a>
                    <a class="dropdown-item logout" href="/login">Log Out</a>

                  </div>
                </div>
                 <!--  <a class="btn btn-outline-primary lg" href="/login">Masuk</a> -->
              </li>
              </ul>
          </div>
          </div>
      </nav>
    <!-- akhir navbar -->
  
    </body>


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script type="text/javaScript" src="js/bootstrap.min.js" ></script>
    <script type="text/javaScript" src="js/bootstrap.min.js" ></script>
  </body>
</html>
