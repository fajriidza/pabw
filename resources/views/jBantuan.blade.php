<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Jemput Bantuan </title>

    <!-- Bootstrap -->
    <link href="vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- bootstrap-wysiwyg -->
    <link href="vendors/google-code-prettify/bin/prettify.min.css" rel="stylesheet">
    <!-- Select2 -->
    <link href="vendors/select2/dist/css/select2.min.css" rel="stylesheet">
    <!-- Switchery -->
    <link href="vendors/switchery/dist/switchery.min.css" rel="stylesheet">
    <!-- starrr -->
    <link href="vendors/starrr/dist/starrr.css" rel="stylesheet">
    <!-- bootstrap-daterangepicker -->
    <link href="vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
    <!-- Datatables -->
    <link href="vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">
    <!-- Custom Theme Style -->
    <link href="build/css/custom.min.css" rel="stylesheet">
    <link href="css/gaya.css" rel="stylesheet">
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="index.html" class="site_title"><i class="fa fa-cube"></i> <span>DONATE</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="img/user.png" alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Petugas,</span>
                <h2>M. Sulthon Alif</h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>General</h3>
                <ul class="nav side-menu">
                  <li><a><i class="fa fa-check-square-o"></i> Verifikasi <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="/verifDonasi">Verifikasi Donatur & Donasi</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-automobile"></i> Jemput <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="/jBantuan">Jemput Bantuan</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-cube"></i> Distribusi <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="/diBantuan">Distribusi Bantuan</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-users"></i> Data <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="/dDonatur">Data Donatur</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-info-circle"></i> Informasi <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="/infBencana">Informasi Bencana</a></li>
                    </ul>
                  </li>
                </ul>
              </div>
            </div>
            <!-- /sidebar menu -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="img/user.png" alt="">M. Sulthon Alif
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    <li><a href="/loginPetugas"><i class="fa fa-sign-out pull-right"></i> Keluar</a></li>
                  </ul>
                </li>
              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">

              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h3>Jemput Bantuan</h3>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <!-- start project list -->
                    <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th style="width: 1%">Kode Donasi</th>
                          <th style="width: 20%">Nama Donatur</th>
                          <th>Alamat</th>
                          <th>Tanggal Jemput</th>
                          <th>Waktu Jemput</th>
                          <th>Status</th>
                          <th style="width: 20%">Opsi</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>1</td>
                          <td>Dendy Surya Darmawan</td>
                          <td>Kalimantan Tengah</td>
                          <td>20/12/2018</td>
                          <td>07.00</td>
                          <td>Telah Dijemput</td>
                          <td>
                            <a href="editJadwal" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> Atur Jadwal Penjemputan </a>
                          </td>
                        </tr>
                        <tr>
                          <td>1</td>
                          <td>Dendy Surya Darmawan</td>
                          <td>Kalimantan Tengah</td>
                          <td>20/12/2018</td>
                          <td>07.00</td>
                          <td>Telah Dijemput</td>
                          <td>
                            <a href="editJadwal" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> Atur Jadwal Penjemputan </a>
                          </td>
                        </tr>
                        <tr>
                          <td>1</td>
                          <td>Dendy Surya Darmawan</td>
                          <td>Kalimantan Tengah</td>
                          <td>20/12/2018</td>
                          <td>07.00</td>
                          <td>Telah Dijemput</td>
                          <td>
                            <a href="editJadwal" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> Atur Jadwal Penjemputan </a>
                          </td>
                        </tr>
                        <tr>
                          <td>1</td>
                          <td>Dendy Surya Darmawan</td>
                          <td>Kalimantan Tengah</td>
                          <td>20/12/2018</td>
                          <td>07.00</td>
                          <td>Telah Dijemput</td>
                          <td>
                            <a href="editJadwal" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> Atur Jadwal Penjemputan </a>
                          </td>
                        </tr>
                        <tr>
                          <td>1</td>
                          <td>Dendy Surya Darmawan</td>
                          <td>Kalimantan Tengah</td>
                          <td>20/12/2018</td>
                          <td>07.00</td>
                          <td>Telah Dijemput</td>
                          <td>
                            <a href="editJadwal" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> Atur Jadwal Penjemputan </a>
                          </td>
                        </tr>
                        <tr>
                          <td>1</td>
                          <td>Dendy Surya Darmawan</td>
                          <td>Kalimantan Tengah</td>
                          <td>20/12/2018</td>
                          <td>07.00</td>
                          <td>Telah Dijemput</td>
                          <td>
                            <a href="editJadwal" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> Atur Jadwal Penjemputan </a>
                          </td>
                        </tr>
                        <tr>
                          <td>1</td>
                          <td>Dendy Surya Darmawan</td>
                          <td>Kalimantan Tengah</td>
                          <td>20/12/2018</td>
                          <td>07.00</td>
                          <td>Telah Dijemput</td>
                          <td>
                            <a href="editJadwal" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> Atur Jadwal Penjemputan </a>
                          </td>
                        </tr>

                      </tbody>
                    </table>
                    <!-- end project list -->

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
            Teknik Informatika UII - Admin Design by De Nun
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="vendors/nprogress/nprogress.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- iCheck -->
    <script src="vendors/iCheck/icheck.min.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="vendors/moment/min/moment.min.js"></script>
    <script src="vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
    <!-- bootstrap-wysiwyg -->
    <script src="vendors/bootstrap-wysiwyg/js/bootstrap-wysiwyg.min.js"></script>
    <script src="vendors/jquery.hotkeys/jquery.hotkeys.js"></script>
    <script src="vendors/google-code-prettify/src/prettify.js"></script>
    <!-- jQuery Tags Input -->
    <script src="vendors/jquery.tagsinput/src/jquery.tagsinput.js"></script>
    <!-- Switchery -->
    <script src="vendors/switchery/dist/switchery.min.js"></script>
    <!-- Select2 -->
    <script src="vendors/select2/dist/js/select2.full.min.js"></script>
    <!-- Parsley -->
    <script src="vendors/parsleyjs/dist/parsley.min.js"></script>
    <!-- Autosize -->
    <script src="vendors/autosize/dist/autosize.min.js"></script>
    <!-- jQuery autocomplete -->
    <script src="vendors/devbridge-autocomplete/dist/jquery.autocomplete.min.js"></script>
    <!-- starrr -->
    <script src="vendors/starrr/dist/starrr.js"></script>
    <!-- Datatables -->
    <script src="vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="vendors/jszip/dist/jszip.min.js"></script>
    <script src="vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="vendors/pdfmake/build/vfs_fonts.js"></script>
    <!-- Custom Theme Scripts -->
    <script src="/build/js/custom.min.js"></script>

  </body>
</html>
