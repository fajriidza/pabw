<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBOYROYFc-XcfFCMmw5MVlOZc1Tuh_HC2U"
  type="text/javascript"></script>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="/fontawesome/css/all.css">
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/css/navbarlogin.css">
    <link rel="stylesheet" href="/css/formDonasi.css">

    <title>Form Donasi</title>
  </head>
     <body>
       <!-- navbar -->
       <nav class="navbar fixed-top navbar-expand-lg navbar-light bg-light">
           <div class="container-fluid">
           <a class="navbar-brand" href="/">DONATE</a>
           <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
               <span class="navbar-toggler-icon"></span>
           </button>
           <div class="collapse navbar-collapse" id="navbarNav">
               <ul class="navbar-nav ml-auto">
               <li class="nav-item">
                   <a class="nav-link" href="/welcome">Beranda <span class="sr-only">(current)</span></a>
               </li>
               <li class="nav-item">
                   <a class="nav-link" href="/panduanDonasi">Panduan Donasi</a>
               </li>
               <li class="nav-item">
                   <a class="btn btn-primary ds" href="/donatur/donasi">Donasi Sekarang</a>
               </li>
               @if (!session('key'))
               <li class="nav-item">
                 <div class="btn-group lg" role="group">
                   <button id="btnGroupDrop1" type="button" class="btn dropdown-toggle btn-outline-primary lg" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                     Masuk
                   </button>
                   <div class="dropdown-menu dropdown-menu-right" aria-labelledby="btnGroupDrop1">
                     <a class="dropdown-item" href="/loginD">Donatur</a>
                     <a class="dropdown-item" href="/loginPetugas">Petugas</a>
                   </div>
                 </div>
                  <!--  <a class="btn btn-outline-primary lg" href="/login">Masuk</a> -->
               </li>
               @else
               <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    @if(Auth::user())
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                    @endif
                                    @if(Auth::guard('petugas')->user())
                                    {{Auth::guard('petugas')->user()->name}}
                                    @endif
                                    @if(Auth::guard('admin')->user())
                                    {{Auth::guard('admin')->user()->name}}
                                    @endif

                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
            @endif
               </ul>
           </div>
           </div>
       </nav>
       <!-- akhir navbar -->

    <!-- form donasi -->
    <section class="formDonasi">
      <div class="bg">
        <div class="overlay">
        <div class="row">
          <div class="col-md-8 offset-md-2 form">
            <h1>Form Donasi</h1>
            @if(session()->has('notif'))
            <div class="alert alert-success">
                <button type="button" class="close" data-dismiss="alert-success" aria-label="true">
                  <span aria-hidden="true">&times;</span></button>
                  <strong>Berhasil !! </strong>{{session()->get('notif')}}
            </div>
            @endif
             <form class="" action="{{route('tambah.donasi') }}" method="post">
              {{ csrf_field() }}
              <div class="row">
                <div class="col-md">
                  <h6>Data Donasi</h6>
                  <label for="">Nama Donasi</label>
                  <input type="text" name="nama_donasi" class="form-control" required>
                </div>
                <div class="col-md">
                  <h6>Lokasi Penjemputan</h6>
                  <label for="">Nama Donatur</label>
                  <input type="text" name="" value="" class="form-control" placeholder="M. Sulthon Alif" readonly>
                </div>
              </div>
              <div class="row">
                <div class="col-md">
                  <label for="">Jenis Donasi</label>
                  <select name="jenis_donasi" id="" class="form-control" required>
                        <option value="" disabled selected hidden>Pilih Jenis Donasi</option>
                        @foreach($categoris as $cat)
                        <option value="<?=$cat->id?>"><?=$cat->kategori_nama?></option>
                        @endforeach
                    </select>
                      <label for="">Jumlah Donasi</label>
                    <input type="number" name="jumlah_donasi" class="form-control" required>
                    </div>
                <div class="col-md">
                  <label for="">Alamat Lengkap</label>
                  <textarea name="name"  class="form-control alamatTxt" readonly></textarea>
                </div>
              </div>

              <div class="row">
                <div class="col-md">
                  <label for="">Bencana</label>
                  <input type="text" name="nama_bencana" value="{{$bencana->nama_bencana}}"class="form-control" readonly>
                </div>
                <div class="col-md">
                  <label for="">Provinsi Donatur</label>
                  <select class="form-control" name="" readonly>
                    <option value=""></option>
                  </select>
                </div>
              </div>
              <div class="row">
                <div class="col-md">
                  <label for="">Provinsi</label>
                    <input type="text" name="" value="{{$bencana->regencies->provinces['name']}}"class="form-control" readonly>
                </div>
                <div class="col-md">
                  <label for="">Kabupaten/Kota Donatur</label>
                  <select class="form-control" name="" readonly>
                    <option value=""></option>
                  </select>
                </div>
              </div>
              <div class="row">
                <div class="col-md">
                 <label for="">Kabupaten/Kota</label>
                  <input type="text" name="" value="{{$bencana->regencies['name']}}"class="form-control" readonly>
                  <input type="hidden" name="lokasi_id" value="{{$bencana->lokasi_id}}"class="form-control" readonly>
                  <input type="hidden" name="status" value="Komitmen"class="form-control" readonly>
                  <input type="hidden" name="bencana_id" value="{{$bencana->id}}" class="form-control" readonly>
                  <input type="hidden" name="pemilik_id" value="{{ Auth::user()->id }}" class="form-control" readonly>
                </div>

                <div class="col-md">
                  <label for="">Kecamatan Donatur</label>
                  <select class="form-control" name="" readonly>
                    <option value=""></option>
                  </select>
                </div>
              </div>
              <div class="row">
                <div class="col-md">
                   <label for="">Keterangan Donasi</label>
                  <textarea name="deskripsi_donasi" class="form-control" required style="resize: none"></textarea>
                </div>
              </div> <br>
              <button type="submit" name="button" class="col-md-4 offset-md-4 btn btn-primary btnDonasi btn-block">Donasi</button>
            </form>
          </div>
        </div>
        <footer>
          <div class="row">
            <div class="col-12">
              <p class="text-center">&copy; Copyright 2018 | Built with by <span>De Nun </span><br>Jalan Kaliurang Km. 14,5, Yogyakarta, Krawitan, Umbulmartani, Ngemplak, Kabupaten Sleman, <br>Daerah Istimewa Yogyakarta 55584 </p>
            </div>
          </div>
        </footer>

      </div>

      </div>
    </section>
    <!-- form donasi -->
    <!-- footer -->

    <!-- akhir footer -->

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="/js/jquery-3.3.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script type="text/javaScript" src="/js/bootstrap.min.js" ></script>
  </body>
</html>
