<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
@if (session()->has('alert'))
                  <h1>
                      {{ session('alert') }}
                  </h1>
              @endif

<div class="section">
  <form action="{{ route('bencana.store') }}" method="POST" enctype="multipart/form-data">
    {!! csrf_field() !!}

        <div class="form-group">
            <label for="nama">Nama Bencana</label>
                <input type="text" class="form-control" id="nama" placeholder="Masukkan Nama Bencana" name="NamaBencana" required>
                <input type="hidden" name="petugas_id" value="{{Auth::guard('petugas')->user()->id}}">

        </div>
        <div class="form-group">
            <label for="nama">Deskripsi Bencana</label>
                <input type="text" class="form-control" id="nama" placeholder="Masukkan Deskripsi Bencana" name="deskripsi_bencana" required>
        </div>
        <div class="form-group">
        <label for="kebutuhan">Kebutuhan Bencana</label>
            <input type="checkbox" id="kebutuhan" name="kebutuhan[]" value="Sandang">Sandang<br>
            <input type="checkbox" id="kebutuhan" name="kebutuhan[]" value="Pangan">Pangan<br>
            <input type="checkbox" id="kebutuhan" name="kebutuhan[]" value="Papan">Papan<br>
        </div>
        <div class="form-group">
             <label for="">Lokasi Pengiriman</label>
                {!! Form::select('id_province',[''=>'--- Pilih Provinsi ---']+$provinces,null,['class'=>'form-control','required']) !!}
        </div>
        <div class="form-group">
            <label>Pilih Kabupaten/Kota:</label>
                {!! Form::select('id_regencies',[''=>'--- Pilih Kabupaten/Kota ---'],null,['class'=>'form-control','required']) !!}
        </div>

      <div class="row">
        <div class="col s6">
            <img src="http://placehold.it/100x100" id="showgambar" style="max-width:100px;max-height:100px;float:left;" />
        </div>
    </div>
    <div class="row">
        <div class="input-field col s6">
          <input type="file" id="inputgambar" name="gambar" class="validate"/ accept="image/*" / required >
          <br>
        </div>
      </div>
      <button type="submit" class="btn btn-flat pink accent-3 waves-effect waves-light white-text right">Submit <i class="material-icons right">send</i></button>
  </form>
</div>
<br>
<br>
<br>
<div class="container">
    <div class="col-lg-12">
    <table class="table justify-content-start mt-5">
    <thead>
    <tr>
      <th scope="col">Gambar</th>
      <th scope="col">Nama Bencana </th>
      <th scope="col">Deskripsi </th>
      <th scope="col">Lokasi Lokasi Bencana</th>
      <th scope="col">Kebutuhan</th>
     <th scope="col">Waktu Ditambah</th>
      <th scope="col" width="140">Opsi</th>
    </tr>
    </thead>
    @foreach($bencana as $bencana)
     <tr>
      <td><img src="{{ asset('img/bencana/'.$bencana->gambar)  }}" style="max-height:100px;max-width:100px;margin-top:10px;"></td>
      <td>{{$bencana->nama_bencana}}</td>
      <td>{{$bencana->deskripsi_bencana}}</td>
      <td>{{$bencana->regencies['name']}} , {{$bencana->regencies->provinces['name']}}</td>
      <td>{{$bencana->kebutuhan}}</td>
     <td>{{$bencana->created_at}}</td>

      <td><a href="" class="btn btn-primary btn-sm" style="float:left;">Edit</a>
                <form action="" method="post">
                  {{ csrf_field() }}
                  {{ method_field('DELETE') }}
                  <input type="hidden" name="_method" value="DELETE">
                  <button type="submit" class="btn btn-danger btn-sm" style="margin-left:4px;">Hapus</button>
                </form>
      </td>
    </tr>
     @endforeach
    </table>
</div>
</div>

<!-- Script ajax pilih Lokasi-->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
  <script type="text/javascript">
    $("select[name='id_province']").change(function(){
        var id_province = $(this).val();
        var token = $("input[name='_token']").val();
        $.ajax({
            url: "<?php echo route('select-ajax') ?>",
            method: 'POST',
            data: {id_province:id_province, _token:token},
            success: function(data) {
              $("select[name='id_regencies'").html('');
              $("select[name='id_regencies'").html(data.options);
            }
        });
    });
  </script>

<!--script ajax upload gambar-->
<!--
<script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
<script type="text/javascript">
        (function($){
    $(function(){
        $('.button-collapse').sideNav();
    });
        })(jQuery);
    
</script> -->
<script type="text/javascript">

      function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#showgambar').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#inputgambar").change(function () {
        readURL(this);
    });

</script>
</body>
</html>