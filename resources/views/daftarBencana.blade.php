<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="fontawesome/css/all.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/navbarlogin.css">
    <link rel="stylesheet" href="css/daftarBencana.css">

    <title>Daftar Bencana</title>
  </head>
     <body>
       <!-- navbar -->
       <nav class="navbar fixed-top navbar-expand-lg navbar-light bg-light">
           <div class="container-fluid">
           <a class="navbar-brand" href="#">DONATE</a>
           <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
               <span class="navbar-toggler-icon"></span>
           </button>
           <div class="collapse navbar-collapse" id="navbarNav">
               <ul class="navbar-nav ml-auto">
               <li class="nav-item">
                   <a class="nav-link" href="/welcome">Beranda <span class="sr-only">(current)</span></a>
               </li>
               <li class="nav-item">
                   <a class="nav-link" href="#">Tentang Kami</a>
               </li>
               <li class="nav-item">
                   <a class="nav-link" href="#">Panduan Donasi</a>
               </li>
               <li class="nav-item">
                   <a class="btn btn-primary ds" href="/formdonasi">Donasi Sekarang</a>
               </li>
               <li class="nav-item">
                 <div class="btn-group lg" role="group">
                   <button id="btnGroupDrop1" type="button" class="btn dropdown-toggle btn-outline-primary lg" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                     Masuk
                   </button>
                   <div class="dropdown-menu dropdown-menu-right" aria-labelledby="btnGroupDrop1">
                     <a class="dropdown-item" href="/login">Donatur</a>
                     <a class="dropdown-item" href="/loginPetugas">Petugas</a>
                   </div>
                 </div>
                  <!--  <a class="btn btn-outline-primary lg" href="/login">Masuk</a> -->
               </li>
               </ul>
           </div>
           </div>
       </nav>
       <!-- akhir navbar -->

    <!-- detail bencana -->
    <div class="jumbotron jumbotron-fluid">
      <div class="container-fluid">
        <div class="row">
        <div class="col-md-10 offset-md-1">
          <h2 class="text-center">Daftar Bencana</h2>
          <div class="row">
          <div class="col-md">
            <a href="/detailBencana"><img src="img/7.jpg" alt="" class="img-fluid shadow p-1 mb-2 bg-white rounded"></a>
            <a href="/detailBencana"> <h6 class="text-center">Gunung Meletus</h6></a>
          </div>
          <div class="col-md">
            <a href="/detailBencana">
              <img src="img/6.jpg" class="img-fluid shadow p-1 mb-2 bg-white rounded" alt=""></a>
            <a href="/detailBencana"><h6 class="text-center">Tornado</h6></a>
          </div>
          <div class="col-md">
            <a href="/detailBencana"><img src="img/8.jpg" class="img-fluid shadow p-1 mb-2 bg-white rounded" alt=""></a>
            <a href="/detailBencana"> <h6 class="text-center">Banjir</h6></a>
          </div>
        </div>

        <div class="row">
        <div class="col-md">
          <a href="/detailBencana"><img src="img/7.jpg" alt="" class="img-fluid shadow p-1 mb-2 bg-white rounded"></a>
          <a href="/detailBencana"> <h6 class="text-center">Gunung Meletus</h6></a>
        </div>
        <div class="col-md">
          <a href="/detailBencana">
            <img src="img/6.jpg" class="img-fluid shadow p-1 mb-2 bg-white rounded" alt=""></a>
          <a href="/detailBencana"><h6 class="text-center">Tornado</h6></a>
        </div>
        <div class="col-md">
          <a href="/detailBencana"><img src="img/8.jpg" class="img-fluid shadow p-1 mb-2 bg-white rounded" alt=""></a>
          <a href="/detailBencana"> <h6 class="text-center">Banjir</h6></a>
        </div>
      </div>
      <a href="#" class="col-md-2 offset-md-5 btn btn-primary btn-block btnBerikut">Berikutnya</a>
        </div>

      </div>
      <!-- footer -->
      <footer>
        <div class="row">
          <div class="col-10 offset-1 text-center">
            <p>&copy; Copyright 2018 | Built with by <span>De Nun </span><br>Jalan Kaliurang Km. 14,5, Yogyakarta, Krawitan, Umbulmartani, Ngemplak, Kabupaten Sleman, <br>Daerah Istimewa Yogyakarta 55584 </p>
          </div>
        </div>
      </footer>
      <!-- akhir footer -->
      </div>
    </div>
    <!-- akhir detail bencana -->



    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script type="text/javaScript" src="js/bootstrap.min.js" ></script>
  </body>
</html>
