<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Gentelella Alela! | </title>

    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- bootstrap-wysiwyg -->
    <link href="../vendors/google-code-prettify/bin/prettify.min.css" rel="stylesheet">
    <!-- Select2 -->
    <link href="../vendors/select2/dist/css/select2.min.css" rel="stylesheet">
    <!-- Switchery -->
    <link href="../vendors/switchery/dist/switchery.min.css" rel="stylesheet">
    <!-- starrr -->
    <link href="../vendors/starrr/dist/starrr.css" rel="stylesheet">
    <!-- bootstrap-daterangepicker -->
    <link href="../vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="../build/css/custom.min.css" rel="stylesheet">
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="index.html" class="site_title"><i class="fa fa-paw"></i> <span>Gentelella Alela!</span></a>
            </div>

            <div class="clearfix"></div>

           @include('Petugas.sidebarPetugas')

        <!-- Data Donatur -->
        <div class="modal fade" id="dataDonatur" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Detail Data Donatur</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <form class="form-signin">
                  <div class="row">
                    <div class="col-md-6">
                      <h5>Data Donatur</h5>
                      <div class="form-group">
                        <label for="inputName">Nama Lengkap</label>
                        <input type="text" id="inputName" class="form-control" placeholder="M. Sulthon Alif" required readonly>
                      </div>

                      <div class="form-group">
                        <label for="inputEmail">Email</label>
                        <input type="email" id="inputEmail" class="form-control" placeholder="17523069@students.uii.ac.id" required readonly>
                      </div>

                      <div class="form-group">
                        <label for="inputHp">No. Telepon</label>
                        <input type="number" id="inputHp" class="form-control" placeholder="081320009000" required readonly>
                      </div>

                       <div class="form-group">
                        <label for="inputAlamat">Alamat Lengkap</label>
                        <input type="text" id="inputAlamat" class="form-control" placeholder="Kos Latahzan" required readonly>
                      </div>

                      <div class="form-group">
                       <label for="inputAlamat">Provinsi</label>
                       <input type="text" id="inputAlamat" class="form-control" placeholder="Kalimantan Tengah" required readonly>
                     </div>

                     <div class="form-group">
                      <label for="inputAlamat">Kabupaten/Kota</label>
                      <input type="text" id="inputAlamat" class="form-control" placeholder="Kab. Kotawaringin Barat" required readonly>
                    </div>

                    <div class="form-group">
                     <label for="inputAlamat">Kecamatan</label>
                     <input type="text" id="inputAlamat" class="form-control" placeholder="Kec. Kumai" required readonly>
                   </div>
                    </div>
                    <div class="col-md-6">
                      <h5>Data Donasi</h5>
                      <div class="form-group">
                        <label for="inputHp">Jenis Bantuan</label>
                        <input type="number" id="inputHp" class="form-control" placeholder="Pangan" required readonly>
                      </div>
                      <div class="form-group">
                        <label for="inputHp">Nama Barang</label>
                        <input type="number" id="inputHp" class="form-control" placeholder="Indomie Goreng" required readonly>
                      </div>
                      <div class="form-group">
                        <label for="inputHp">Jumlah</label>
                        <input type="number" id="inputHp" class="form-control" placeholder="20" required readonly>
                      </div>
                      <div class="form-group">
                        <label for="inputName">Jadwal Penjemputan</label>
                        <input type="date" id="inputJadwal" class="form-control" placeholder="10/11/2018" required readonly>
                      </div>
                       <div class="form-group">
                        <label for="inputAlamat">Waktu Penjemputan</label>
                        <input type="time" id="inputWaktu" class="form-control" placeholder="14.00" required readonly>
                      </div>
                      <div class="form-group">
                        <label for="inputHp">Status</label>
                        <input type="text" id="inputHp" class="form-control" placeholder="Telah Dijemput" required readonly>
                      </div>
                    </div>
                  </div>
                </form>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              </div>
            </div>
          </div>
        </div>
        <!-- Akhir Data Donatur -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>DONATE <small>Data Donatur</small></h3>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span>
                  </div>
                </div>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>DONATE</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <!-- start project list -->
                    <table class="table table-striped projects">
                      <thead>
                        <tr>
                          <th style="width: 10%">Kode Donasi</th>
                          <th style="width: 15%">Nama Donatur</th>
                          <th>Jenis Bantuan</th>
                          <th>Nama Barang</th>
                          <th>Jumlah</th>
                          <th>Status</th>
                          <th style="width: 20%">Opsi</th>
                        </tr>
                      </thead>
                         @foreach($donasi as $donasi)
                        <tr>
                          <td>{{$donasi->kode_donasi}}</td>
                          <td>{{$donasi->get_dataUser['name']}}</td>
                          <td>{{$donasi->get_jenis_donasi['kategori_nama']}}</td>
                          <td>{{$donasi->nama_donasi}}</td>
                          <td>{{$donasi->jumlah_donasi}}</td>
                          <td>{{$donasi->status}}</td>
                          <td>
                            <a href="#" class="btn btn-primary btn-xs" alt="" data-toggle="modal" data-target="#dataDonatur" data-whatever="@mdo"><i class="fa fa-pencil"></i> Detail </a>
                          </td>
                        </tr>
                       @endforeach()
                      </tbody>
                    </table>
                    <!-- end project list -->

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
            Gentelella - Bootstrap Admin Template by <a href="https://colorlib.com">Colorlib</a>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="../vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../vendors/nprogress/nprogress.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="../vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- iCheck -->
    <script src="../vendors/iCheck/icheck.min.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="../vendors/moment/min/moment.min.js"></script>
    <script src="../vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
    <!-- bootstrap-wysiwyg -->
    <script src="../vendors/bootstrap-wysiwyg/js/bootstrap-wysiwyg.min.js"></script>
    <script src="../vendors/jquery.hotkeys/jquery.hotkeys.js"></script>
    <script src="../vendors/google-code-prettify/src/prettify.js"></script>
    <!-- jQuery Tags Input -->
    <script src="../vendors/jquery.tagsinput/src/jquery.tagsinput.js"></script>
    <!-- Switchery -->
    <script src="../vendors/switchery/dist/switchery.min.js"></script>
    <!-- Select2 -->
    <script src="../vendors/select2/dist/js/select2.full.min.js"></script>
    <!-- Parsley -->
    <script src="../vendors/parsleyjs/dist/parsley.min.js"></script>
    <!-- Autosize -->
    <script src="../vendors/autosize/dist/autosize.min.js"></script>
    <!-- jQuery autocomplete -->
    <script src="../vendors/devbridge-autocomplete/dist/jquery.autocomplete.min.js"></script>
    <!-- starrr -->
    <script src="../vendors/starrr/dist/starrr.js"></script>
    <!-- Custom Theme Scripts -->
    <script src="../build/js/custom.min.js"></script>

  </body>
</html>
