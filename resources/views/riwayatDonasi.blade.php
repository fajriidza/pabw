<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/riwayat.css">
    <link rel="stylesheet" href="fontawesome/css/all.css">

    <title>Riwayat Donasi</title>
  </head>
     <body>
      <!-- navbar -->
      <nav class="navbar fixed-top navbar-expand-lg navbar-light bg-light">
          <div class="container-fluid">
          <a class="navbar-brand" href="#">DONATE</a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarNav">
              <ul class="navbar-nav ml-auto">
              <li class="nav-item">
                  <a class="nav-link" href="/welcome">Beranda <span class="sr-only">(current)</span></a>
              </li>
              <li class="nav-item">
                  <a class="nav-link" href="/panduanDonasi">Panduan Donasi</a>
              </li>
              <li class="nav-item">
                  <a class="btn btn-outline-primary ds" href="/formdonasi">Donasi Sekarang</a>
              </li>
              <li class="nav-item">
                <div class="btn-group lg" role="group">
                  <button id="btnGroupDrop1" type="button" class="btn dropdown-toggle btn-primary lg" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="fas fa-user-circle"></i>  Mr. Ahmad Daidfaids
                  </button>
                  <div class="dropdown-menu dropdown-menu-right" aria-labelledby="btnGroupDrop1">
                    <a class="dropdown-item" href="/editProfil">Edit Profil</a>
                    <a class="dropdown-item" href="/riwayatDonasi">Riwayat Donasi</a>
                    <a class="dropdown-item logout" href="/login">Keluar</a>

                  </div>
                </div>
                 <!--  <a class="btn btn-outline-primary lg" href="/login">Masuk</a> -->
              </li>
              </ul>
          </div>
          </div>
      </nav>
    <!-- akhir navbar -->

    <!-- Detail Donasi -->
    <div class="modal fade" id="detailDonasi" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title" id="exampleModalLabel">Distribusi Bantuan</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form class="form-signin">
              <div class="row">
                <div class="col-md-6">
                  <h5>Data Donasi</h5>
                  <div class="form-group">
                    <label for="inputEmail">Id</label>
                    <input type="text" id="inputEmail" class="form-control" placeholder="KB41AA" required readonly>
                  </div>

                  <div class="form-group">
                    <label for="inputName">Nama Donatur</label>
                    <input type="text" id="inputName" class="form-control" placeholder="M. Sulthon Alif" required readonly>
                  </div>

                  <div class="form-group">
                    <label for="inputHp">Jenis Bantuan</label>
                    <input type="text" id="inputHp" class="form-control" placeholder="Papan" required readonly>
                  </div>

                   <div class="form-group">
                    <label for="inputAlamat">Nama Barang</label>
                    <input type="text" id="inputAlamat" class="form-control" placeholder="Tenda Sedang" required readonly>
                  </div>

                  <div class="form-group">
                   <label for="inputAlamat">Jumlah</label>
                   <input type="number" id="inputAlamat" class="form-control" placeholder="25" required readonly>
                 </div>

                </div>
                <div class="col-md-6">
                  <h5>Lokasi Tujuan</h5>
                  <div class="form-group">
                   <label for="inputAlamat">Provinsi</label>
                   <input type="text" id="inputAlamat" class="form-control" placeholder="Kalimantan Tengah" required readonly>
                 </div>

                  <div class="form-group">
                   <label for="inputAlamat">Kabupaten/Kota</label>
                   <input type="text" id="inputAlamat" class="form-control" placeholder="Kab. Kotawaringin Barat" required readonly>
                 </div>

                 <div class="form-group">
                  <label for="inputAlamat">Kecamatan</label>
                  <input type="text" id="inputAlamat" class="form-control" placeholder="Kec. Kumai" required readonly>
                </div>
                </div>
              </div>
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>
    <!-- Detail Donasi -->

      <div class="bg">
        <div class="overlay">

      <div class="row">

        <div class="col-sm-8 offset-sm-2 form ">
              <h2 class="text-center">Riwayat Donasi</h2>

              <table class="table">
                <thead>
                  <tr>
                    <th scope="col">Jenis Bantuan</th>
                    <th scope="col">Nama Barang</th>
                    <th scope="col">Jumlah</th>
                    <th scope="col">Keterangan</th>
                    <th scope="col">Status</th>
                    <th scope="col">Detail</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>Sandang</td>
                    <td>Pakaian Anak</td>
                    <td>30</td>
                    <td>Untuk umur kira-kira 5-12 tahun</td>
                    <td>Distribusi</td>
                    <td>
                      <a href="#" class="btn btn-secondary btn-xs" alt="" data-toggle="modal" data-target="#detailDonasi" data-whatever="@mdo"><i class="fas fa-info-circle"></i> Detail</a>
                    </td>
                  </tr>
                  <tr>
                    <td>Sandang</td>
                    <td>Pakaian Anak</td>
                    <td>30</td>
                    <td>Untuk umur kira-kira 5-12 tahun</td>
                    <td>Distribusi</td>
                    <td>
                      <a href="#" class="btn btn-secondary btn-xs" alt="" data-toggle="modal" data-target="#detailDonasi" data-whatever="@mdo"><i class="fas fa-info-circle"></i> Detail</a>
                    </td>
                  </tr>
                  <tr>
                    <td>Sandang</td>
                    <td>Pakaian Anak</td>
                    <td>30</td>
                    <td>Untuk umur kira-kira 5-12 tahun</td>
                    <td>Distribusi</td>
                    <td>
                      <a href="#" class="btn btn-secondary btn-xs" alt="" data-toggle="modal" data-target="#detailDonasi" data-whatever="@mdo"><i class="fas fa-info-circle"></i> Detail</a>
                    </td>
                  </tr>
                  <tr>
                    <td>Sandang</td>
                    <td>Pakaian Anak</td>
                    <td>30</td>
                    <td>Untuk umur kira-kira 5-12 tahun</td>
                    <td>Distribusi</td>
                    <td>
                      <a href="#" class="btn btn-secondary btn-xs" alt="" data-toggle="modal" data-target="#detailDonasi" data-whatever="@mdo"><i class="fas fa-info-circle"></i> Detail</a>
                    </td>
                  </tr>
                  <tr>
                    <td>Sandang</td>
                    <td>Pakaian Anak</td>
                    <td>30</td>
                    <td>Untuk umur kira-kira 5-12 tahun</td>
                    <td>Distribusi</td>
                    <td>
                      <a href="#" class="btn btn-secondary btn-xs" alt="" data-toggle="modal" data-target="#detailDonasi" data-whatever="@mdo"><i class="fas fa-info-circle"></i> Detail</a>
                    </td>
                  </tr>
                  <tr>
                    <td>Sandang</td>
                    <td>Pakaian Anak</td>
                    <td>30</td>
                    <td>Untuk umur kira-kira 5-12 tahun</td>
                    <td>Distribusi</td>
                    <td>
                      <a href="#" class="btn btn-secondary btn-xs" alt="" data-toggle="modal" data-target="#detailDonasi" data-whatever="@mdo"><i class="fas fa-info-circle"></i> Detail</a>
                    </td>
                  </tr>
                  <tr>
                    <td>Sandang</td>
                    <td>Pakaian Anak</td>
                    <td>30</td>
                    <td>Untuk umur kira-kira 5-12 tahun</td>
                    <td>Distribusi</td>
                    <td>
                      <a href="#" class="btn btn-secondary btn-xs" alt="" data-toggle="modal" data-target="#detailDonasi" data-whatever="@mdo"><i class="fas fa-info-circle"></i> Detail</a>
                    </td>
                  </tr>
                  <tr>
                    <td>Sandang</td>
                    <td>Pakaian Anak</td>
                    <td>30</td>
                    <td>Untuk umur kira-kira 5-12 tahun</td>
                    <td>Distribusi</td>
                    <td>
                      <a href="#" class="btn btn-secondary btn-xs" alt="" data-toggle="modal" data-target="#detailDonasi" data-whatever="@mdo"><i class="fas fa-info-circle"></i> Detail</a>
                    </td>
                  </tr>
                  <tr>
                    <td>Sandang</td>
                    <td>Pakaian Anak</td>
                    <td>30</td>
                    <td>Untuk umur kira-kira 5-12 tahun</td>
                    <td>Distribusi</td>
                    <td>
                      <a href="#" class="btn btn-secondary btn-xs" alt="" data-toggle="modal" data-target="#detailDonasi" data-whatever="@mdo"><i class="fas fa-info-circle"></i> Detail</a>
                    </td>
                  </tr>
                  <tr>
                    <td>Sandang</td>
                    <td>Pakaian Anak</td>
                    <td>30</td>
                    <td>Untuk umur kira-kira 5-12 tahun</td>
                    <td>Distribusi</td>
                    <td>
                      <a href="#" class="btn btn-secondary btn-xs" alt="" data-toggle="modal" data-target="#detailDonasi" data-whatever="@mdo"><i class="fas fa-info-circle"></i> Detail</a>
                    </td>
                  </tr>
                  <tr>
                    <td>Sandang</td>
                    <td>Pakaian Anak</td>
                    <td>30</td>
                    <td>Untuk umur kira-kira 5-12 tahun</td>
                    <td>Distribusi</td>
                    <td>
                      <a href="#" class="btn btn-secondary btn-xs" alt="" data-toggle="modal" data-target="#detailDonasi" data-whatever="@mdo"><i class="fas fa-info-circle"></i> Detail</a>
                    </td>
                  </tr>
                  <tr>
                    <td>Sandang</td>
                    <td>Pakaian Anak</td>
                    <td>30</td>
                    <td>Untuk umur kira-kira 5-12 tahun</td>
                    <td>Distribusi</td>
                    <td>
                      <a href="#" class="btn btn-secondary btn-xs" alt="" data-toggle="modal" data-target="#detailDonasi" data-whatever="@mdo"><i class="fas fa-info-circle"></i> Detail</a>
                    </td>
                  </tr>
                </tbody>
              </table>


            </div>
            </div>

                 <!-- footer -->
                 <footer>
                   <div class="row">
                     <div class="col-10 offset-1 text-center">
                       <p>&copy; Copyright 2018 | Built with by <span>De Nun </span><br>Jalan Kaliurang Km. 14,5, Yogyakarta, Krawitan, Umbulmartani, Ngemplak, Kabupaten Sleman, <br>Daerah Istimewa Yogyakarta 55584 </p>
                     </div>
                   </div>
                 </footer>
                <!-- akhir footer -->
              </div>
            </div>

    </body>


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script type="text/javaScript" src="js/bootstrap.min.js" ></script>
    <script type="text/javaScript" src="js/bootstrap.min.js" ></script>
  </body>
</html>
