<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="/fontawesome/css/all.css">
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/css/navbarlogin.css">
    <link rel="stylesheet" href="/css/detailBencana.css">

    <title>Detail Bencana</title>
  </head>
     <body>
        <!-- navbar -->
    <nav class="navbar fixed-top navbar-expand-lg navbar-light bg-light">
        <div class="container-fluid">
        <a class="navbar-brand" href="/">DONATE</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav ml-auto">
            <li class="nav-item active">
                <a class="nav-link" href="/">Beranda <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/panduanDonasi">Panduan Donasi</a>
            </li>
            <li class="nav-item">
                <a class="btn btn-outline-primary ds" href="/donatur/donasi">Donasi Sekarang</a>
            </li>
            @if (!session('key'))
            <li class="nav-item">
              <div class="btn-group lg" role="group">
                <button id="btnGroupDrop1" type="button" class="btn dropdown-toggle btn-outline-primary lg" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  Masuk
                </button>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="btnGroupDrop1">
                  <a class="dropdown-item" href="/login">Donatur</a>
                  <a class="dropdown-item" href="{{ route('petugas.login') }}">Petugas</a>
                </div>
              </div>
               <!--  <a class="btn btn-outline-primary lg" href="/login">Masuk</a> -->
            </li>
             @else
              <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    @if(Auth::user())
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                    @endif
                                    @if(Auth::guard('petugas')->user())
                                    {{Auth::guard('petugas')->user()->name}}
                                    @endif
                                    @if(Auth::guard('admin')->user())
                                    {{Auth::guard('admin')->user()->name}}
                                    @endif

                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
            @endif
            </ul>
        </div>
        </div>
    </nav>
    <!-- akhir navbar -->

    <!-- detail bencana -->
      <div class="jumbotron jumbotron-fluid">
        <div class="container-fluid">


        <div class="row">
          <div class="col-md-10 offset-md-1">
            <h1 >{{$bencana->nama_bencana}}</h1>
            <div class="row">
              <div class="col-md-6">
                <img src="{{ asset('img/bencana/'.$bencana->gambar)  }}" class="img-fluid shadow p-1 mb-2 bg-white rounded" alt="" style="height:300px;width:550px;">
                <div class="lokasi">
                  <a href="#" class="col-md-12"><i class="fas fa-map-marker-alt"></i>Lokasi Bencana</a>
                </div>
              </div>
              <div class="col-md-6">
                <h4>Deskripsi</h4>
                <p>      Kecamatan Cangkringan menjadi lokasi terparah yang terkena dampak erupsi dahsyat Merapi pada Jumat (5/11/2010) dini hari tadi. Hampir seluruh korban, termasuk satu korban tewas, berasal dari beberapa desa di kecamatan tersebut.

Sri Lestari, warga Cangkringan, menceritakan bagaimana sergapan awan panas seolah memburu warga di tengah pekatnya malam karena matinya penerangan.

Sehari sebelum erupsi, Rabu (4/11/2010), menurut Sri, petugas gabungan sebenarnya telah mengumumkan untuk bersiap mengungsi pada Jumat, hari ini. Pun rupanya, erupsi terjadi lebih awal dari yang telah diingatkan. Untuk Saat ini Kecamatan Cangkringan memerlukan bantuan berupa Pangan.
</p>
                <a href="/formdonasi"><button type="submit" name="button" class="col-md-4 offset-md-4 btn btn-primary btnDonasi"><i class="fas fa-hand-holding-usd fa-lg"></i>  Donasi</button></a>
                  <div class="row">
                    <div class="col-md-4 offset-md-4 d-flex justify-content-center">
                    </div>
                  </div>
              </div>
          </div>

        </div>

      </div>
      <footer>
        <div class="row">
          <div class="col-10 offset-1 text-center">
            <p>&copy; Copyright 2018 | Built with by <span>De Nun </span><br>Jalan Kaliurang Km. 14,5, Yogyakarta, Krawitan, Umbulmartani, Ngemplak, Kabupaten Sleman, <br>Daerah Istimewa Yogyakarta 55584 </p>
          </div>
        </div>
      </footer>
        </div>

      </div>
    <!-- akhir detail bencana -->

    <!-- footer -->

    <!-- akhir footer -->

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script type="text/javaScript" src="js/bootstrap.min.js" ></script>
  </body>
</html>
