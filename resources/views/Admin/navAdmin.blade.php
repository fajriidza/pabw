<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="au theme template">
    <meta name="author" content="Hau Nguyen">
    <meta name="keywords" content="au theme template">

    <!-- Title Page-->
    <title>Dashboard</title>

    <!-- Fontfaces CSS-->
    <link href="/css/font-face.css" rel="stylesheet" media="all">
    <link href="/dashboardadmin/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    <link href="/dashboardadmin/font-awesome-5/css/fontawesome-all.min.css" rel="stylesheet" media="all">
    <link href="/dashboardadmin/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">

    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link href="/dashboardadmin/bootstrap-4.1/bootstrap.min.css" rel="stylesheet" media="all">

    <!-- Vendor CSS-->
    <link href="/dashboardadmin/animsition/animsition.min.css" rel="stylesheet" media="all">
    <link href="/dashboardadmin/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet" media="all">
    <link href="dashboardadmin/wow/animate.css" rel="stylesheet" media="all">
    <link href="dashboardadmin/css-hamburgers/hamburgers.min.css" rel="stylesheet" media="all">
    <link href="dashboardadmin/slick/slick.css" rel="stylesheet" media="all">
    <link href="dashboardadmin/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="dashboardadmin/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="dashboardadmin/theme.css" rel="stylesheet" media="all">

</head>

<body class="animsition">
    <div class="page-wrapper">
        <!-- HEADER MOBILE-->
        <header class="header-mobile d-block d-lg-none">
            <div class="header-mobile__bar">
                <div class="container-fluid">
                    <div class="header-mobile-inner">
                        <a class="logo" href="index.html">
                            <img src="img/icon/logo.png" alt="CoolAdmin" />
                        </a>
                        <button class="hamburger hamburger--slider" type="button">
                            <span class="hamburger-box">
                                <span class="hamburger-inner"></span>
                            </span>
                        </button>
                    </div>
                </div>
            </div>
            <nav class="navbar-mobile">
                <div class="container-fluid">
                    <ul class="navbar-mobile__list list-unstyled">
                        <li class="active has-sub">
                            <a class="js-arrow" href="/admin">
                                <i class="fas fa-tachometer-alt"></i>Dashboard</a>
                        </li>
                        <li>
                            <a href="/kelolaPetugas">
                              <i class="fas fa-tachometer-alt"></i>Kelola Petugas</a>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <!-- END HEADER MOBILE-->

        <!-- MENU SIDEBAR-->
        <aside class="menu-sidebar d-none d-lg-block">
            <div class="logo">
                <a href="#">
                    <img src="img/icon/logo.png" alt="Cool Admin" />
                </a>
            </div>
            <div class="menu-sidebar__content js-scrollbar1">
                <nav class="navbar-sidebar">
                    <ul class="list-unstyled navbar__list">
                        <li class="has-sub">
                            <a class="js-arrow" href="/admin">
                                <i class="fas fa-tachometer-alt"></i>Dashboard</a>
                        <li class="has-sub">
                            <a href="/kelolaPetugas">
                                <i class="fas fa-tachometer-alt"></i>Kelola Petugas</a>
                        </li>
                        </li>
                    </ul>
                </nav>
            </div>
        </aside>
        <!-- END MENU SIDEBAR-->

        <!-- PAGE CONTAINER-->
        <div class="page-container">
            <!-- HEADER DESKTOP-->
            <header class="header-desktop">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="header-wrap">
                            <div class="header-button">
                                <div class="account-wrap">
                                    <div class="account-item clearfix js-item-menu">
                                        <div class="image">
                                            <img src="img/icon/avatar-01.jpg" alt="John Doe" />
                                        </div>
                                        <div class="content">
                                            <a class="js-acc-btn" href="">{{Auth::guard('admin')->user()->name}}</a>
                                        </div>
                                        <div class="account-dropdown js-dropdown">
                                            <div class="info clearfix">
                                                <div class="image">
                                                    <a href="#">
                                                        <img src="img/icon/avatar-01.jpg" alt="John Doe" />
                                                    </a>
                                                </div>
                                                <div class="content">
                                                    <h5 class="name">
                                                        <a href="#">{{Auth::guard('admin')->user()->name}}</a>
                                                    </h5>
                                                    <span class="email">{{Auth::guard('admin')->user()->email}}</span>
                                                </div>
                                            </div>
                                             <div class="account-dropdown__footer">
                                                <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    <i class="zmdi zmdi-power"></i>Logout
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
            <!-- HEADER DESKTOP-->
@yield('content')

    <!-- Jquery JS-->
    <script src="dashboardadmin/jquery-3.2.1.min.js"></script>
    <!-- Bootstrap JS-->
    <script src="dashboardadmin/bootstrap-4.1/popper.min.js"></script>
    <script src="dashboardadmin/bootstrap-4.1/bootstrap.min.js"></script>
    <!-- Vendor JS       -->
    <script src="dashboardadmin/slick/slick.min.js">
    </script>
    <script src="dashboardadmin/wow/wow.min.js"></script>
    <script src="dashboardadmin/animsition/animsition.min.js"></script>
    <script src="dashboardadmin/bootstrap-progressbar/bootstrap-progressbar.min.js">
    </script>
    <script src="dashboardadmin/counter-up/jquery.waypoints.min.js"></script>
    <script src="dashboardadmin/counter-up/jquery.counterup.min.js">
    </script>
    <script src="dashboardadmin/circle-progress/circle-progress.min.js"></script>
    <script src="dashboardadmin/perfect-scrollbar/perfect-scrollbar.js"></script>
    <script src="dashboardadmin/chartjs/Chart.bundle.min.js"></script>
    <script src="dashboardadmin/select2/select2.min.js">
    </script>

    <!-- Main JS-->
    <script src="js/main.js"></script>

<!-- Modal Edit Petugas-->
    <script>
$('#editPetugas').on('show.bs.modal',function (event) {
    var button = $(event.relatedTarget)
    var nama = button.data('nama') //sesuai data modal
    var alamat = button.data('alamat')
    var email = button.data('email')
    var hp = button.data('hp') 
    var id = button.data('id') 
    var modal = $(this)
    modal.find('.modal-body #inputName').val(nama); // #inputName sesuai id dari form input
    modal.find('.modal-body #inputAlamat').val(alamat);
    modal.find('.modal-body #inputEmail').val(email);
    modal.find('.modal-body #inputHp').val(hp);
    modal.find('.modal-body #petugasId').val(id);
})
</script>
<script>
    $(".nav a").on("click", function() {
  $(".nav").find(".active").removeClass("active");
  $(this).parent().addClass("active");
});
</script>

</body>

</html>
<!-- end document-->
