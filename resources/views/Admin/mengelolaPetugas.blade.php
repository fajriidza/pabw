@extends('Admin.navAdmin')
@section('content')
            <!-- Tambah Petugas -->
            <div class="modal fade" id="tambahPetugas" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Tambah Petugas</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <form class="form-signin" method="POST" action="/kelolaPetugas">
                        {{ csrf_field() }}
                      <div class="form-label-group" style="line-height:10px;">
                        <label for="inputName">Nama Lengkap</label>
                        <input type="text" id="inputName" name="nama" class="form-control" placeholder="Nama" required>
                      </div>
                      <br>
                       <div class="form-label-group" style="line-height:10px;">
                        <label for="inputAlamat">Alamat</label>
                        <input type="text" id="inputAlamat" name="alamat" class="form-control" placeholder="Alamat" required>
                      </div>
                      <br style="line-height:10px;">
                      <div class="form-label-group" style="line-height:10px;">
                        <label for="inputEmail">Email address</label>
                        <input type="email" id="inputEmail" name="email" class="form-control" placeholder="Email address" required>
                      </div>
                      <br>
                      <div class="form-label-group" style="line-height:10px;">
                        <label for="inputHp">No. Handphone</label>
                        <input type="text" id="inputHp" name="hp" class="form-control" placeholder="No. Handphone" required>
                      </div>
                      <br>
                      <div class="form-label-group" style="line-height:10px;">
                        <label for="inputPassword">Password</label>
                        <input type="password" id="inputPassword" name="password" class="form-control" placeholder="Password" required>
                      </div>
                      <br>
                      <div class="form-label-group" style="line-height:10px;">
                        <label for="inputRePassword">Re-Password</label>
                        <input type="password" id="inputRePassword" name="repassword" class="form-control" placeholder="Re-Password" required>
                      </div>
                       <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Tambah</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                  </div>
                    </form>
                  </div>
                 
                </div>
              </div>
            </div>
            <!-- Akhir Tambah Petugas -->




            <!-- MAIN CONTENT-->
            <div class="main-content">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col">
                                <div>
                                    <h2 class="title-1 m-b-25">Daftar Petugas DONATE</h2>
                                    <button type="button" class="btn btn-success" data-toggle="modal" data-target="#tambahPetugas" data-whatever="@mdo">Tambah Petugas</button>
                                    <br>
                                    <br>
                                </div>
                                <div class="table-responsive table--no-card m-b-40">
                                    <table class="table table-borderless table-striped table-earning">
                                        <thead class="">
                                            <tr class="">
                                                <th>Nama</th>
                                                <th>Alamat</th>
                                                <th>Email</th>
                                                <th>No.Hp</th>
                                                <th>Opsi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($Petugas as $petugas)
                                            <tr>
                                                <td>{{$petugas->name}}</td>
                                                <td>{{$petugas->alamat}}</td>
                                                <td>{{$petugas->email}}</td>
                                                <td>{{$petugas->hp}}</td>
                                                <td>
                                                
                                                  <button type="button" class="btn btn-primary btn-sm" data-id="{{$petugas->id}}" data-nama="{{$petugas->name}}" data-alamat="{{$petugas->alamat}}" data-email="{{$petugas->email}}" data-hp="{{$petugas->hp}}" data-toggle="modal" data-target="#editPetugas">Edit </button> 

                                            
                                                    
                                                <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#hapusPetugas">Hapus</button>
                                                    
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END MAIN CONTENT-->
<!-- Edit Petugas -->

            <div class="modal fade" id="editPetugas" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <div id="coba"></div>
                    <h5 class="modal-title" id="exampleModalLabel">Edit Petugas</h5> 
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <form class="form-signin" method="POST" action="{{ route('petugas.update','kirim') }}">
                         {{ csrf_field() }}  
                         {{ method_field('PATCH') }}  
                         <input type="hidden" name="petugasId" id="petugasId" value="">
                      <div class="form-label-group" style="line-height:10px;">
                        <label for="inputName">Nama Lengkap</label>
                        <input type="text" id="inputName" name="nama" class="form-control" required>
                      </div>
                      <br>
                       <div class="form-label-group" style="line-height:10px;">
                        <label for="inputAlamat">Alamat</label>
                        <input type="text" id="inputAlamat" name="alamat" class="form-control" placeholder="Alamat" required>
                      </div>
                      <br style="line-height:10px;">
                      <div class="form-label-group" style="line-height:10px;">
                        <label for="inputEmail">Email address</label>
                        <input type="email" id="inputEmail" name="email" class="form-control" placeholder="Email address" required>
                      </div>
                      <br>
                      <div class="form-label-group" style="line-height:10px;">
                        <label for="inputHp">No. Handphone</label>
                        <input type="text" id="inputHp" name="hp" class="form-control" placeholder="No. Handphone" required>
                      </div>
                      <br>
                       <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Simpan</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                  </div>
                    </form>
                  </div>
                 
                </div>
              </div>
            </div>
           
<!-- Akhir Edit Petugas -->

<!-- Hapus Petugas --> 
  <div id="hapusPetugas" class="modal fade">
    <div class="modal-dialog">
      <div class="modal-content">
        <form action="{{ route('petugas.destroy',[$petugas->id]) }}" method="post">
                  {{ csrf_field() }}
                  {{ method_field('DELETE') }}
          <div class="modal-header">            
            <h4 class="modal-title">Hapus Petugas</h4>
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          </div>
          <div class="modal-body">          
            <p>Apakah anda yakin untuk menghapus petugas?</p>
          </div>
          <div class="modal-footer">
            <input type="submit" class="btn btn-danger" value="Ya">
            <input type="button" class="btn btn-default" data-dismiss="modal" value="Tidak">
            
          </div>
        </form>
      </div>
    </div>
  </div>
<!-- akhir hapus Petugas-->
@endsection()