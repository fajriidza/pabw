<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Informasi Bencana</title>

    <!-- Bootstrap -->
    <link href="vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- bootstrap-wysiwyg -->
    <link href="vendors/google-code-prettify/bin/prettify.min.css" rel="stylesheet">
    <!-- Select2 -->
    <link href="vendors/select2/dist/css/select2.min.css" rel="stylesheet">
    <!-- Switchery -->
    <link href="vendors/switchery/dist/switchery.min.css" rel="stylesheet">
    <!-- starrr -->
    <link href="vendors/starrr/dist/starrr.css" rel="stylesheet">
    <!-- bootstrap-daterangepicker -->
    <link href="vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
    <!-- Datatables -->
    <link href="vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">
    <!-- Custom Theme Style -->
    <link href="build/css/custom.min.css" rel="stylesheet">
    <link href="css/gaya.css" rel="stylesheet">
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="index.html" class="site_title"><i class="fa fa-cube"></i> <span>DONATE</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="img/user.png" alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Petugas,</span>
                <h2>M. Sulthon Alif</h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>General</h3>
                <ul class="nav side-menu">
                  <li><a><i class="fa fa-check-square-o"></i> Verifikasi <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="/verifDonasi">Verifikasi Donatur & Donasi</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-automobile"></i> Jemput <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="/jBantuan">Jemput Bantuan</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-cube"></i> Distribusi <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="/diBantuan">Distribusi Bantuan</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-users"></i> Data <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="/dDonatur">Data Donatur</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-info-circle"></i> Informasi <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="/infBencana">Informasi Bencana</a></li>
                    </ul>
                  </li>
                </ul>
              </div>
            </div>
            <!-- /sidebar menu -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="img/user.png" alt="">M. Sulthon Alif
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    <li><a href="/loginPetugas"><i class="fa fa-sign-out pull-right"></i> Keluar</a></li>
                  </ul>
                </li>
              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        <!-- Tambah Bencana -->
        <div class="modal fade" id="tambahBencana" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel">Tambah Informasi Bencana</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <form class="form-signin">
                  <div class="form-group">
                    <label for="inputName">Nama Bencana</label>
                    <input type="text" id="inputName" class="form-control" placeholder="Masukkan Nama Bencana" required>
                  </div>

                  <div class="form-group">
                    <label for="inputName">Provinsi</label>
                    <input type="text" id="inputName" class="form-control" placeholder="Masukkan Provinsi" required>
                  </div>

                  <div class="form-group">
                    <label for="inputName">Kabupaten/Kota</label>
                    <input type="text" id="inputName" class="form-control" placeholder="Masukkan Kabupaten/Kota" required>
                  </div>

                  <div class="form-group">
                    <label for="inputName">Kecamatan</label>
                    <input type="text" id="inputName" class="form-control" placeholder="Masukkan Kecamatan" required>
                  </div>

                  <div class="form-group">
                    <label for="deskripsi-text" class="">Deskripsi</label>
                    <textarea class="form-control" placeholder="Masukkan Deskripsi Bencana" id="deskripsi-text"></textarea>
                  </div>

                  <div class="form-group">
                    <label for="" class="">Gambar</label>
                    <input type="file" class="custom-file-input" id="inputGroupFile02" accept="image/jpeg image/gif image/x-png">
                  </div>
                </form>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                <button type="button" class="btn btn-success">Tambah</button>
              </div>
            </div>
          </div>
        </div>
        <!-- Akhir Tambah Bencana -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <!-- <h3>DONATE <small>Informasi Bencana</small></h3> -->
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h3>Informasi Bencana</h3>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <button type="button" class="btn btn-success" data-toggle="modal" data-target="#tambahBencana" data-whatever="@mdo">Tambah Bencana</button>
                    <!-- start project list -->
                    <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th style="width: 1%">Id</th>
                          <th style="width: 15%">Nama Bencana</th>
                          <th>Provinsi</th>
                          <th>Kabupaten/Kota</th>
                          <th>Kecamatan</th>
                          <th style="width: 15%">Opsi</th>
                          <th>Deskripsi</th>
                          <th>Gambar</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>1</td>
                          <td><a href="/detailBencana">Gempa Bumi</a></td>
                          <td>Kalimantan Tengah</td>
                          <td>Kotawaringin</td>
                          <td>Kumai</td>
                          <td>
                            <a href="/editInfBencana" class="btn btn-info btn-xs" alt=""><i class="fa fa-pencil"></i> Edit </a>
                            <a href="#" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Delete </a>
                          </td>
                          <td>Wah pokoknya parah beut disana hancur banyak rumah dan banyak yang meninggal terus pada hilang orang sama mengungsi ke tenda pengungsian</td>
                          <td><img src="img/favicon.ico" alt="" width="150" height="75"> </td>
                        </tr>
                        <tr>
                          <td>1</td>
                          <td><a href="/detailBencana">Gempa Bumi</a></td>
                          <td>Kalimantan Tengah</td>
                          <td>Kotawaringin</td>
                          <td>Kumai</td>
                          <td>
                            <a href="/editInfBencana" class="btn btn-info btn-xs" alt=""><i class="fa fa-pencil"></i> Edit </a>
                            <a href="#" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Delete </a>
                          </td>
                          <td>Wah pokoknya parah beut disana hancur banyak rumah dan banyak yang meninggal terus pada hilang orang sama mengungsi ke tenda pengungsian</td>
                          <td><img src="img/favicon.ico" alt="" width="150" height="75"> </td>
                        </tr>
                        <tr>
                          <td>1</td>
                          <td><a href="/detailBencana">Gempa Bumi</a></td>
                          <td>Kalimantan Tengah</td>
                          <td>Kotawaringin</td>
                          <td>Kumai</td>
                          <td>
                            <a href="/editInfBencana" class="btn btn-info btn-xs" alt=""><i class="fa fa-pencil"></i> Edit </a>
                            <a href="#" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Delete </a>
                          </td>
                          <td>Wah pokoknya parah beut disana hancur banyak rumah dan banyak yang meninggal terus pada hilang orang sama mengungsi ke tenda pengungsian</td>
                          <td><img src="img/favicon.ico" alt="" width="150" height="75"> </td>
                        </tr>
                        <tr>
                          <td>1</td>
                          <td><a href="/detailBencana">Gempa Bumi</a></td>
                          <td>Kalimantan Tengah</td>
                          <td>Kotawaringin</td>
                          <td>Kumai</td>
                          <td>
                            <a href="/editInfBencana" class="btn btn-info btn-xs" alt=""><i class="fa fa-pencil"></i> Edit </a>
                            <a href="#" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Delete </a>
                          </td>
                          <td>Wah pokoknya parah beut disana hancur banyak rumah dan banyak yang meninggal terus pada hilang orang sama mengungsi ke tenda pengungsian</td>
                          <td><img src="img/favicon.ico" alt="" width="150" height="75"> </td>
                        </tr>
                        <tr>
                          <td>1</td>
                          <td><a href="/detailBencana">Gempa Bumi</a></td>
                          <td>Kalimantan Tengah</td>
                          <td>Kotawaringin</td>
                          <td>Kumai</td>
                          <td>
                            <a href="/editInfBencana" class="btn btn-info btn-xs" alt=""><i class="fa fa-pencil"></i> Edit </a>
                            <a href="#" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Delete </a>
                          </td>
                          <td>Wah pokoknya parah beut disana hancur banyak rumah dan banyak yang meninggal terus pada hilang orang sama mengungsi ke tenda pengungsian</td>
                          <td><img src="img/favicon.ico" alt="" width="150" height="75"> </td>
                        </tr>
                        <tr>
                          <td>1</td>
                          <td><a href="/detailBencana">Gempa Bumi</a></td>
                          <td>Kalimantan Tengah</td>
                          <td>Kotawaringin</td>
                          <td>Kumai</td>
                          <td>
                            <a href="/editInfBencana" class="btn btn-info btn-xs" alt=""><i class="fa fa-pencil"></i> Edit </a>
                            <a href="#" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Delete </a>
                          </td>
                          <td>Wah pokoknya parah beut disana hancur banyak rumah dan banyak yang meninggal terus pada hilang orang sama mengungsi ke tenda pengungsian</td>
                          <td><img src="img/favicon.ico" alt="" width="150" height="75"> </td>
                        </tr>
                        <tr>
                          <td>1</td>
                          <td><a href="/detailBencana">Gempa Bumi</a></td>
                          <td>Kalimantan Tengah</td>
                          <td>Kotawaringin</td>
                          <td>Kumai</td>
                          <td>
                            <a href="/editInfBencana" class="btn btn-info btn-xs" alt=""><i class="fa fa-pencil"></i> Edit </a>
                            <a href="#" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Delete </a>
                          </td>
                          <td>Wah pokoknya parah beut disana hancur banyak rumah dan banyak yang meninggal terus pada hilang orang sama mengungsi ke tenda pengungsian</td>
                          <td><img src="img/favicon.ico" alt="" width="150" height="75"> </td>
                        </tr>
                        <tr>
                          <td>1</td>
                          <td><a href="/detailBencana">Gempa Bumi</a></td>
                          <td>Kalimantan Tengah</td>
                          <td>Kotawaringin</td>
                          <td>Kumai</td>
                          <td>
                            <a href="/editInfBencana" class="btn btn-info btn-xs" alt=""><i class="fa fa-pencil"></i> Edit </a>
                            <a href="#" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Delete </a>
                          </td>
                          <td>Wah pokoknya parah beut disana hancur banyak rumah dan banyak yang meninggal terus pada hilang orang sama mengungsi ke tenda pengungsian</td>
                          <td><img src="img/favicon.ico" alt="" width="150" height="75"> </td>
                        </tr>
                        <tr>
                          <td>1</td>
                          <td><a href="/detailBencana">Gempa Bumi</a></td>
                          <td>Kalimantan Tengah</td>
                          <td>Kotawaringin</td>
                          <td>Kumai</td>
                          <td>
                            <a href="/editInfBencana" class="btn btn-info btn-xs" alt=""><i class="fa fa-pencil"></i> Edit </a>
                            <a href="#" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Delete </a>
                          </td>
                          <td>Wah pokoknya parah beut disana hancur banyak rumah dan banyak yang meninggal terus pada hilang orang sama mengungsi ke tenda pengungsian</td>
                          <td><img src="img/favicon.ico" alt="" width="150" height="75"> </td>
                        </tr>

                      </tbody>
                    </table>
                    <!-- end project list -->

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
            Teknik Informatika UII - Admin Design by De Nun
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="vendors/nprogress/nprogress.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- iCheck -->
    <script src="vendors/iCheck/icheck.min.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="vendors/moment/min/moment.min.js"></script>
    <script src="vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
    <!-- bootstrap-wysiwyg -->
    <script src="vendors/bootstrap-wysiwyg/js/bootstrap-wysiwyg.min.js"></script>
    <script src="vendors/jquery.hotkeys/jquery.hotkeys.js"></script>
    <script src="vendors/google-code-prettify/src/prettify.js"></script>
    <!-- jQuery Tags Input -->
    <script src="vendors/jquery.tagsinput/src/jquery.tagsinput.js"></script>
    <!-- Switchery -->
    <script src="vendors/switchery/dist/switchery.min.js"></script>
    <!-- Select2 -->
    <script src="vendors/select2/dist/js/select2.full.min.js"></script>
    <!-- Parsley -->
    <script src="vendors/parsleyjs/dist/parsley.min.js"></script>
    <!-- Autosize -->
    <script src="vendors/autosize/dist/autosize.min.js"></script>
    <!-- jQuery autocomplete -->
    <script src="vendors/devbridge-autocomplete/dist/jquery.autocomplete.min.js"></script>
    <!-- starrr -->
    <script src="vendors/starrr/dist/starrr.js"></script>
    <!-- Datatables -->
    <script src="vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="vendors/jszip/dist/jszip.min.js"></script>
    <script src="vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="vendors/pdfmake/build/vfs_fonts.js"></script>
    <!-- Custom Theme Scripts -->
    <script src="build/js/custom.min.js"></script>

  </body>
</html>
