<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="fontawesome/css/all.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/navbarlogin.css">
    <link rel="stylesheet" href="/css/panduan.css">

    <title>Panduan Donasi</title>
  </head>
     <body>
       <!-- navbar -->
       <nav class="navbar fixed-top navbar-expand-lg navbar-light bg-light">
           <div class="container-fluid">
           <a class="navbar-brand" href="#">DONATE</a>
           <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
               <span class="navbar-toggler-icon"></span>
           </button>
           <div class="collapse navbar-collapse" id="navbarNav">
               <ul class="navbar-nav ml-auto">
               <li class="nav-item">
                   <a class="nav-link" href="/welcome">Beranda <span class="sr-only">(current)</span></a>
               </li>
               <li class="nav-item">
                   <a class="nav-link" href="#">Tentang Kami</a>
               </li>
               <li class="nav-item active">
                   <a class="nav-link" href="#">Panduan Donasi</a>
               </li>
               <li class="nav-item">
                   <a class="btn btn-outline-primary ds" href="/formdonasi">Donasi Sekarang</a>
               </li>
               <li class="nav-item">
                 <div class="btn-group lg" role="group">
                   <button id="btnGroupDrop1" type="button" class="btn dropdown-toggle btn-outline-primary lg" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                     Masuk
                   </button>
                   <div class="dropdown-menu dropdown-menu-right" aria-labelledby="btnGroupDrop1">
                     <a class="dropdown-item" href="/login">Donatur</a>
                     <a class="dropdown-item" href="/loginPetugas">Petugas</a>
                   </div>
                 </div>
                  <!--  <a class="btn btn-outline-primary lg" href="/login">Masuk</a> -->
               </li>
               </ul>
           </div>
           </div>
       </nav>
       <!-- akhir navbar -->

    <!-- detail bencana -->
    <div class="bg">
      <div class="overlay">
        <div class="row">
        <div class="col-md-8 offset-md-2">
          <h2 class="text-center">Panduan Donasi</h2>

            <h6><i class="fas fa-check"></i>Langkah 1 Akses Halaman Website</h6>
            <h6><i class="fas fa-check"></i>Langkah 2 Masuk Akun :</h6>
            <p>a. Klik tombol masuk lalu pilih kolom donatur makan akan masuk ke login donatur, jika punya akun maka tinggal isi email dan password
			      <br>b. Klik tombol masuk lalu pilih kolom donatur makan akan masuk ke login donatur, jika tidak punya akun maka klik tombol daftar lalu isi form daftar yang ada</p>
            <h6><i class="fas fa-check"></i>Langkah 3 Form Donasi :</h6>
            <p>a. Klik tombol "Donasi Sekarang" lalu isi form jika lokasi donasi yang diinginkan ditentukan LSM
			      <br>b. Klik judul bencana pada "Bencana terkini" maka akan di pindahkan ke halaman detail bencana lalu klik donasi lalu isi form jika lokasi donasi yang diinginkan sesuai dengan lokasi bencana tersebut</p>
            <h6><i class="fas fa-check"></i>Langkah 4 Selesai :</h6>
            <p> Jika sudah mengisi form donasi klik donasi lalu akan muncul notifikasi berhasil maka form telah masuk ke LSM yang akan diverifikasi terlebih dahulu</p>
            <h6><i class="fas fa-check"></i>Kategori Status :</h6>
            <p> a. "Menunggu" berarti form donasi yang telah diisi belum diverifikasi oleh petugas
            <br> b. "Komitmen" berarti form donasi yang telah diisi sudah diverifikasi oleh petugas
            <br> c. "Siap Jemput" berarti donasi yang sudah diverifikasi akan dijemput oleh petugas pada tanggal dan waktu yang telah ditentukan
            <br> d. "Telah Dijemput" berarti donasi yang sudah dijemput oleh petugas sudah disimpan di gudang penyimpanan DONATE
            <br> e. "Distribusi" berarti donasi yang disimpan di gudang penyimpanan DONATE akan didistribusikan ke tempat bencana
            <br> f.  Untuk lebih rincinya bisa dilihat di riwayat Donasi milik masing-masing akun Donatur</p>
        </div>
        </div>
        <footer>
          <div class="row">
            <div class="col-10 offset-1 text-center">
              <p>&copy; Copyright 2018 | Built with by <span>De Nun </span><br>Jalan Kaliurang Km. 14,5, Yogyakarta, Krawitan, Umbulmartani, Ngemplak, Kabupaten Sleman, <br>Daerah Istimewa Yogyakarta 55584 </p>
            </div>
          </div>
        </footer>
      </div>
      <!-- footer -->

      <!-- akhir footer -->
      </div>
    </div>
    <!-- akhir detail bencana -->



    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script type="text/javaScript" src="js/bootstrap.min.js" ></script>
  </body>
</html>
