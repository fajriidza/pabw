<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Jemput Bantuan</title>

    <!-- Bootstrap -->
    <link href="vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- bootstrap-wysiwyg -->
    <link href="vendors/google-code-prettify/bin/prettify.min.css" rel="stylesheet">
    <!-- Select2 -->
    <link href="vendors/select2/dist/css/select2.min.css" rel="stylesheet">
    <!-- Switchery -->
    <link href="vendors/switchery/dist/switchery.min.css" rel="stylesheet">
    <!-- starrr -->
    <link href="vendors/starrr/dist/starrr.css" rel="stylesheet">
    <!-- bootstrap-daterangepicker -->
    <link href="vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="build/css/custom.min.css" rel="stylesheet">
    <link href="css/gaya.css" rel="stylesheet">
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="index.html" class="site_title"><i class="fa fa-cube"></i> <span>DONATE</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="img/user.png" alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Petugas,</span>
                <h2>M. Sulthon Alif</h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>General</h3>
                <ul class="nav side-menu">
                  <li><a><i class="fa fa-check-square-o"></i> Verifikasi <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="/verifDonasi">Verifikasi Donatur & Donasi</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-automobile"></i> Jemput <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="/jBantuan">Jemput Bantuan</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-cube"></i> Distribusi <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="/diBantuan">Distribusi Bantuan</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-users"></i> Data <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="/dDonatur">Data Donatur</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-info-circle"></i> Informasi <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="/infBencana">Informasi Bencana</a></li>
                    </ul>
                  </li>
                </ul>
              </div>
            </div>
            <!-- /sidebar menu -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="img/user.png" alt="">M. Sulthon Alif
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    <li><a href="/loginPetugas"><i class="fa fa-sign-out pull-right"></i> Keluar</a></li>
                  </ul>
                </li>
              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">

              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h3>Atur Jadwal Penjemputan</h3>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <!-- start project list -->
                    <form class="form-signin">
                      <div class="row">
                        <div class="col-md-4">
                          <h5>Data Donatur</h5>
                          <div class="form-group">
                            <label for="inputName">Nama Lengkap</label>
                            <input type="text" id="inputName" class="form-control" placeholder="M. Sulthon Alif" required readonly>
                          </div>

                          <div class="form-group">
                            <label for="inputEmail">Email</label>
                            <input type="email" id="inputEmail" class="form-control" placeholder="17523069@students.uii.ac.id" required readonly>
                          </div>

                          <div class="form-group">
                            <label for="inputHp">No. Telepon</label>
                            <input type="number" id="inputHp" class="form-control" placeholder="081320009000" required readonly>
                          </div>

                           <div class="form-group">
                            <label for="inputAlamat">Alamat Lengkap</label>
                            <textarea class="form-control" name="alamat" rows="5" cols="80" required readonly></textarea>
                          </div>

                          <div class="form-group">
                           <label for="inputAlamat">Provinsi</label>
                           <input type="text" id="inputProvinsi" class="form-control" placeholder="Kalimantan Tengah" required readonly>
                         </div>

                         <div class="form-group">
                          <label for="inputAlamat">Kabupaten/Kota</label>
                          <input type="text" id="inputKab" class="form-control" placeholder="Kab. Kotawaringin Barat" required readonly>
                        </div>

                        <div class="form-group">
                         <label for="inputAlamat">Kecamatan</label>
                         <input type="text" id="inputKec" class="form-control" placeholder="Kec. Kumai" required readonly>
                       </div>
                        </div>
                        <div class="col-md-4">
                          <h5>Data Donasi</h5>
                          <div class="form-group">
                            <label for="inputHp">Jenis Bantuan</label>
                            <input type="number" id="inputHp" class="form-control" placeholder="Pangan" required readonly>
                          </div>
                          <div class="form-group">
                            <label for="inputHp">Nama Barang</label>
                            <input type="number" id="inputHp" class="form-control" placeholder="Indomie Goreng" required readonly>
                          </div>
                          <div class="form-group">
                            <label for="inputHp">Jumlah</label>
                            <input type="number" id="inputHp" class="form-control" placeholder="20" required readonly>
                          </div>
                          <div class="form-group">
                           <label for="inputAlamat">Keterangan Donasi</label>
                           <textarea class="form-control" name="ketDonasi" rows="5" cols="80" required readonly></textarea>
                         </div>
                         <div class="form-group">
                           <label for="inputName">Jadwal Penjemputan</label>
                           <input type="date" id="inputJadwal" class="form-control" placeholder="" required>
                         </div>
                          <div class="form-group">
                           <label for="inputAlamat">Waktu Penjemputan</label>
                           <input type="time" id="inputWaktu" class="form-control" placeholder="" required>
                         </div>
                         <div class="form-group">
                           <label for="inputStatus">Status</label>
                           <input type="text" id="inputStatus" class="form-control" placeholder="Telah Dijemput" required>
                         </div>
                        </div>
                        <div class="col-md-4">
                          <h5>Data Bencana</h5>
                          <div class="form-group">
                            <label for="inputHp">Nama Bencana</label>
                            <input type="number" id="inputHp" class="form-control" placeholder="Gempa Bumi" required readonly>
                          </div>
                          <div class="form-group">
                           <label for="inputAlamat">Provinsi</label>
                           <input type="text" id="inputAlamat" class="form-control" placeholder="Kalimantan Tengah" required readonly>
                          </div>
                         <div class="form-group">
                          <label for="inputAlamat">Kabupaten/Kota</label>
                          <input type="text" id="inputAlamat" class="form-control" placeholder="Kab. Kotawaringin Barat" required readonly>
                         </div>
                        <div class="form-group">
                         <label for="inputAlamat">Deskripsi</label>
                         <textarea class="form-control" name="ketDonasi" rows="5" cols="80" required readonly></textarea>
                       </div>
                        </div>
                      </div>
                       <a href="/jBantuan"><button type="button" class="btn btn-success">Simpan</button></a>
                       <a href="/jBantuan"><button type="button" class="btn btn-secondary">Tutup</button></a>
                    </form>
                    <!-- end project list -->

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
            Teknik Informatika UII - Admin Design by De Nun
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="vendors/nprogress/nprogress.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- iCheck -->
    <script src="vendors/iCheck/icheck.min.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="vendors/moment/min/moment.min.js"></script>
    <script src="vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
    <!-- bootstrap-wysiwyg -->
    <script src="vendors/bootstrap-wysiwyg/js/bootstrap-wysiwyg.min.js"></script>
    <script src="vendors/jquery.hotkeys/jquery.hotkeys.js"></script>
    <script src="vendors/google-code-prettify/src/prettify.js"></script>
    <!-- jQuery Tags Input -->
    <script src="vendors/jquery.tagsinput/src/jquery.tagsinput.js"></script>
    <!-- Switchery -->
    <script src="vendors/switchery/dist/switchery.min.js"></script>
    <!-- Select2 -->
    <script src="vendors/select2/dist/js/select2.full.min.js"></script>
    <!-- Parsley -->
    <script src="vendors/parsleyjs/dist/parsley.min.js"></script>
    <!-- Autosize -->
    <script src="vendors/autosize/dist/autosize.min.js"></script>
    <!-- jQuery autocomplete -->
    <script src="vendors/devbridge-autocomplete/dist/jquery.autocomplete.min.js"></script>
    <!-- starrr -->
    <script src="vendors/starrr/dist/starrr.js"></script>
    <!-- Custom Theme Scripts -->
    <script src="build/js/custom.min.js"></script>

  </body>
</html>
